# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.12.0
### Fixed
- `OccupiedEntry::into_mut` and `VacantEntry::insert` use generics on functions that need to return a reference with the lifetime of `Self` instead of having a generic lifetime parameter.

## 0.11.0
### Added
- A lint to forbid using unsafe code in the library.
- Implemented `IntoIterator` for references to `Edges`.
- `Graph::with_capacities` now has `must_use` attribute.
- `OccupiedMapEntry` now requires `into_mut` method to be implemented. All supported map types provide this method.
- `OccupiedEntry` gained `into_mut` method.
- `graph::Entry` gained `or_insert` and `or_insert_with` methods.

### Changed
- `algo::dom` no longer uses unsafe code.
- Documentation for methods that may return errors or panic now mostly has corresponding documentation. This is enforced to some degree by clippy lints.
- The immediate dominator search algorithm is changed to store node immediate dominators in their weights using the new `WithImmDom` trait.
- `OccupiedMapEntry` now has a generic lifetime parameter.

### Removed
- `MapEntry` lost its trait bounds and one generic argument.

## 0.10.1
### Added
- A generic parameter for all `HashMap`-related aliases to allow using a non-default hasher.

## 0.10.0
### Changed
- Dropping a draining edge iterator won't cause the remaining edges to be drained.

### Fixed
- Draining edge iterators would cause a graph's state to be invalid when forgotten.

## 0.9.0
### Added
- A lint that prevents `unwrap()` from being used to make panic messages more clear.
- `Nodes::walk_successors` and `Nodes::walk_predecessors` similar to corresponding `FrozenGraph` methods.
- `Graph::try_split_node_at_index` that serves the same purpose as `Graph::split_node` does for graphs with internal key node maps.

### Changed
- `FrozenGraph::edge_weight`, `FrozenGraph::edge_weight_mut` and `Graph::remove_edge` now return `Option` values instead of panicking for consistency with other APIs.

### Fixed
- Fixed warnings when built with some feature configurations.
- `map` module docs had invalid references.

## 0.8.0
### Changed
- `EdgesWalker::walk_next` renamed to `walk_edges_next` to prevent an alias with `Walker::walk_next`.

## 0.7.0

### Added
- Exposed `node` and `node_mut` methods on `FrozenGraph`. This allows to avoid double map lookups when one wants to walk edges after doing something with a node's weight.
- A `cargo-deny` config.
- Support filtering edges when doing SCC discovery.

### Changed
- Added a `'static` bound on `Map::Key` for consistency with other crate parts.

### Removed
- An unnecessary `NI` generic parameter on `Nodes`.

## 0.6.2

### Added
- Added `walk_successors` and `walk_predecessors` methods to `Node`.

## 0.6.1
Updated crate documentation.

## 0.6.0

### Added
- Support for splitting a reference to a graph into a reference to its nodes and a reference to its edges.
- Support for parallel iteration using rayon.
- An implementation of `MapWithEntry` for `HashMap`.
- Integration with [`indexmap`](https://crates.io/crates/indexmap).

### Changed
- `BTreeMap` and `HashMap` now use `std::iter::Map` as the type to dereference key references returned by their iterators instead of custom iterator types to ensure all methods are properly forwarded.
- `slotmap::SecondaryKeySet` renamed to `SecondarySlotKeySet`.
- `slotmap::SparseSecondaryKeySet` renamed to `SparseSecondarySlotKeySet`.
- All type aliases moved into the `aliases` module.
- `btreemap` and `hashmap` modules are now private.
- `slotmap` module moved into the `map` module.

### Fixed
- **MSRV** is now 1.66 since `map_first_last` feature is used. This should be done with the `0.4.1` release.

## 0.5.0

### Added
- Added `weight`, `weight_mut`, `from` and `to` methods to the `Edge` structure.
- Added `WalkInputs` and `WalkOutputs` iterators.

### Changed
- Non-draining iterators over graph's edges now return a reference to `Edge` rather than just the edge weight.
- `Walker::walk_next` can now return values with the lifetime of the graph reference received as its argument.

### Fixed
- `DrainOutgoingEdges` and `DrainIncomingEdges` iterators would live a graph in an invalid state when dropped prematurely.

## 0.4.1
- Added `first_node`, `first_node_mut`, `last_node` and `last_node_mut` methods to `FrozenGraph`.

## 0.4.0
Started the changelog.

### Added
- Entry API for graphs backed by maps providing one.

### Fixed
- Fixed broken `DrainIncomingEdges` iterator.

### Changed
- The old `Graph::remove_node` has been renamed to `unlink_and_remove_node`. The new `remove_node` method returns an error when removing a linked node.
