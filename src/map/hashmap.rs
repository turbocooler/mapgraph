#[cfg(feature = "indexmap")]
use indexmap::{map as index_map, IndexMap, IndexSet};
#[cfg(feature = "std")]
use std::collections::{hash_map, HashMap, HashSet};

macro_rules! impl_hashmap {
    ($map_name:ident, $map_module:ident, $set_name:ident, $remove_method:ident) => {
        impl<K, T, S> $crate::map::Map<T> for $map_name<K, T, S>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            S: ::core::hash::BuildHasher + ::core::default::Default,
        {
            type Key = K;
            type Iter<'a> = $crate::util::KeyMap<'a, $map_module::Iter<'a, K, T>, K, T> where Self: 'a, T: 'a;
            type IterMut<'a> = $crate::util::KeyMapMut<'a, $map_module::IterMut<'a, K, T>, K, T> where Self: 'a, T: 'a;

            fn len(&self) -> usize {
                $map_name::len(self)
            }

            fn is_empty(&self) -> bool {
                $map_name::is_empty(self)
            }

            fn contains_key(&self, index: Self::Key) -> bool {
                $map_name::contains_key(self, &index)
            }

            fn get(&self, index: Self::Key) -> ::core::option::Option<&T> {
                $map_name::get(self, &index)
            }

            fn get_mut(&mut self, index: Self::Key) -> ::core::option::Option<&mut T> {
                $map_name::get_mut(self, &index)
            }

            fn remove(&mut self, index: Self::Key) -> ::core::option::Option<T> {
                $map_name::$remove_method(self, &index)
            }

            fn clear(&mut self) {
                $map_name::clear(self)
            }

            fn iter(&self) -> Self::Iter<'_> {
                ::core::iter::Iterator::map($map_name::iter(self), $crate::util::map_deref_key)
            }

            fn iter_mut(&mut self) -> Self::IterMut<'_> {
                ::core::iter::Iterator::map($map_name::iter_mut(self), $crate::util::map_deref_key)
            }
        }

        impl<K, T, S> $crate::map::MapWithCapacity<T> for $map_name<K, T, S>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            S: ::core::hash::BuildHasher + ::core::default::Default,
        {
            fn with_capacity(capacity: usize) -> Self {
                $map_name::with_capacity_and_hasher(capacity, S::default())
            }

            fn capacity(&self) -> usize {
                $map_name::capacity(self)
            }

            fn reserve(&mut self, additional: usize) {
                $map_name::reserve(self, additional)
            }
        }

        impl<K, T, S> $crate::map::ExtKeyMap<T> for $map_name<K, T, S>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            S: ::core::hash::BuildHasher + ::core::default::Default,
        {
            fn try_insert(&mut self, key: Self::Key, value: T) -> ::core::result::Result<(), $crate::map::OccupiedError> {
                match $map_name::entry(self, key) {
                    $map_module::Entry::Vacant(entry) => {
                        entry.insert(value);
                        ::core::result::Result::Ok(())
                    }
                    $map_module::Entry::Occupied(_) => ::core::result::Result::Err($crate::map::OccupiedError),
                }
            }

            fn replace(&mut self, key: Self::Key, value: T) -> Option<T> {
                $map_name::insert(self, key, value)
            }
        }

        impl<'a, K, V> $crate::map::VacantMapEntry for $map_module::VacantEntry<'a, K, V>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            V: 'a,
        {
            type Value = V;

            fn insert<'b>(self, value: Self::Value) -> &'b mut Self::Value
            where
                Self: 'b,
            {
                $map_module::VacantEntry::insert(self, value)
            }
        }

        impl<'a, K, V> $crate::map::OccupiedMapEntry for $map_module::OccupiedEntry<'a, K, V>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            V: 'a,
        {
            type Value = V;

            fn get(&self) -> &Self::Value {
                $map_module::OccupiedEntry::get(self)
            }

            fn get_mut(&mut self) -> &mut Self::Value {
                $map_module::OccupiedEntry::get_mut(self)
            }

            fn into_mut<'b>(self) -> &'b mut Self::Value
            where
                Self: 'b {
                $map_module::OccupiedEntry::into_mut(self)
            }

            fn replace(&mut self, value: Self::Value) -> Self::Value {
                $map_module::OccupiedEntry::insert(self, value)
            }

            fn remove(self) -> Self::Value {
                $map_module::OccupiedEntry::$remove_method(self)
            }
        }

        impl<K, T, S> $crate::map::MapWithEntry<T> for $map_name<K, T, S>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            S: ::core::hash::BuildHasher + ::core::default::Default,
        {
            type VacantEntry<'a> = $map_module::VacantEntry<'a, K, T> where Self: 'a;
            type OccupiedEntry<'a> = $map_module::OccupiedEntry<'a, K, T> where Self: 'a;

            fn entry(
                &mut self,
                key: Self::Key,
            ) -> $crate::map::MapEntry<Self::OccupiedEntry<'_>, Self::VacantEntry<'_>> {
                match $map_name::entry(self, key) {
                    $map_module::Entry::Vacant(entry) => $crate::map::MapEntry::Vacant(entry),
                    $map_module::Entry::Occupied(entry) => $crate::map::MapEntry::Occupied(entry),
                }
            }
        }

        impl<K, S> $crate::map::KeySet<K> for $set_name<K, S>
        where
            K: ::core::marker::Copy + ::core::cmp::Eq + ::core::hash::Hash + ::core::fmt::Debug + 'static,
            S: ::core::hash::BuildHasher + ::core::default::Default,
        {
            fn len(&self) -> usize {
                $set_name::len(self)
            }

            fn is_empty(&self) -> bool {
                $set_name::is_empty(self)
            }

            fn contains(&self, index: K) -> bool {
                $set_name::contains(self, &index)
            }

            fn insert(&mut self, index: K) -> bool {
                $set_name::insert(self, index)
            }

            fn remove(&mut self, index: K) -> bool {
                $set_name::$remove_method(self, &index)
            }

            fn clear(&mut self) {
                $set_name::clear(self)
            }
        }
    };
}

#[cfg(feature = "std")]
impl_hashmap!(HashMap, hash_map, HashSet, remove);
#[cfg(feature = "indexmap")]
impl_hashmap!(IndexMap, index_map, IndexSet, swap_remove);
