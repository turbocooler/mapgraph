//! Contains implementations of some algorithms that can be used on [`Graph`](crate::Graph)s.

pub mod dfs;
#[cfg(feature = "alloc")]
pub mod dom;
#[cfg(feature = "alloc")]
pub mod scc;
