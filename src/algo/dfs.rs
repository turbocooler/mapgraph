//! Provides implementation of the DFS algorithm.

use crate::{
    graph::{iter::Walker, Edge, Node},
    map::{IntKeyMap, KeySet, Map},
    FrozenGraph,
};
use core::fmt::Debug;

fn dfs_visit_mut_impl<G, N, E, NI, EI, NM, EM, VS, F>(
    graph: &mut G,
    node_index: NI,
    f: &mut F,
    visited: &mut VS,
) where
    G: AsMut<FrozenGraph<N, E, NI, EI, NM, EM>>,
    NI: Copy + Eq + Debug + 'static,
    EI: Copy + Eq + Debug + 'static,
    NM: Map<Node<N, EI>, Key = NI>,
    EM: IntKeyMap<Edge<E, NI, EI>, Key = EI>,
    VS: KeySet<NI>,
    F: FnMut(&mut G, NI),
{
    f(graph, node_index);
    visited.insert(node_index);

    let mut walker = graph.as_mut().walk_successors(node_index);
    while let Some(succ_index) = walker.walk_next(graph.as_mut()) {
        if !visited.contains(succ_index) {
            dfs_visit_mut_impl(graph, succ_index, f, visited);
        }
    }
}

/// Calls a closure for each node in the graph in the DFS order starting at a specified index.
pub fn dfs_visit_mut<G, N, E, NI, EI, NM, EM, VS, F>(
    graph: &mut G,
    idx: NI,
    mut f: F,
    visited: &mut VS,
) where
    G: AsMut<FrozenGraph<N, E, NI, EI, NM, EM>>,
    NI: Copy + Eq + Debug + 'static,
    EI: Copy + Eq + Debug + 'static,
    NM: Map<Node<N, EI>, Key = NI>,
    EM: IntKeyMap<Edge<E, NI, EI>, Key = EI>,
    VS: KeySet<NI>,
    F: FnMut(&mut G, NI),
{
    dfs_visit_mut_impl(graph, idx, &mut f, visited)
}

fn reverse_dfs_visit_mut_impl<G, N, E, NI, EI, NM, EM, VS, F>(
    graph: &mut G,
    node_index: NI,
    f: &mut F,
    visited: &mut VS,
) where
    G: AsMut<FrozenGraph<N, E, NI, EI, NM, EM>>,
    NI: Copy + Eq + Debug + 'static,
    EI: Copy + Eq + Debug + 'static,
    NM: Map<Node<N, EI>, Key = NI>,
    EM: IntKeyMap<Edge<E, NI, EI>, Key = EI>,
    VS: KeySet<NI>,
    F: FnMut(&mut G, NI),
{
    visited.insert(node_index);

    let mut walker = graph.as_mut().walk_successors(node_index);
    while let Some(succ_index) = walker.walk_next(graph.as_mut()) {
        if !visited.contains(succ_index) {
            reverse_dfs_visit_mut_impl(graph, succ_index, f, visited);
        }
    }

    f(graph, node_index);
}

/// Calls a closure for each node in the graph the reverse DFS order starting at a specified index.
pub fn reverse_dfs_visit_mut<G, N, E, NI, EI, NM, EM, VS, F>(
    graph: &mut G,
    idx: NI,
    mut f: F,
    visited: &mut VS,
) where
    G: AsMut<FrozenGraph<N, E, NI, EI, NM, EM>>,
    NI: Copy + Eq + Debug + 'static,
    EI: Copy + Eq + Debug + 'static,
    NM: Map<Node<N, EI>, Key = NI>,
    EM: IntKeyMap<Edge<E, NI, EI>, Key = EI>,
    VS: KeySet<NI>,
    F: FnMut(&mut G, NI),
{
    reverse_dfs_visit_mut_impl(graph, idx, &mut f, visited)
}
