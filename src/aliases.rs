//! Contains type aliases of the [`Graph`] type that use specific node and edge maps and the maps themselves.

use crate::{graph::Node, FrozenGraph, Graph};
#[cfg(feature = "alloc")]
use alloc::collections::BTreeMap;
#[cfg(feature = "std")]
use std::{collections::HashMap, hash::RandomState};
#[cfg(feature = "slotmap")]
use {
    crate::{
        graph::Edge,
        map::slotmap::{EdgeIndex, NodeIndex},
    },
    slotmap::{DenseSlotMap, HopSlotMap, SecondaryMap, SlotMap, SparseSecondaryMap},
};

/// A type alias for a [`SlotMap`] that maps a certain key type (the default is [`NodeIndex`]) to
/// [`Node`] structures.
///
/// This is intended to be used as a node map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type SlotNodeMap<N, NI = NodeIndex, EI = EdgeIndex> = SlotMap<NI, Node<N, EI>>;

/// A type alias for a [`SlotMap`] that maps a certain key type (the default is [`EdgeIndex`]) to
/// [`Edge`] structures.
///
/// This is intended to be used as an edge map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type SlotEdgeMap<E, NI = NodeIndex, EI = EdgeIndex> = SlotMap<EI, Edge<E, NI, EI>>;

/// A type alias for a [`HopSlotMap`] that maps a certain key type (the default is [`NodeIndex`]) to
/// [`Node`] structures.
///
/// This is intended to be used as a node map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type HopSlotNodeMap<N, NI = NodeIndex, EI = EdgeIndex> = HopSlotMap<NI, Node<N, EI>>;

/// A type alias for a [`HopSlotMap`] that maps a certain key type (the default is [`EdgeIndex`]) to
/// [`Edge`] structures.
///
/// This is intended to be used as an edge map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type HopSlotEdgeMap<E, NI = NodeIndex, EI = EdgeIndex> = HopSlotMap<EI, Edge<E, NI, EI>>;

/// A type alias for a [`DenseSlotMap`] that maps a certain key type (the default is [`NodeIndex`]) to
/// [`Node`] structures.
///
/// This is intended to be used as a node map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type DenseSlotNodeMap<N, NI = NodeIndex, EI = EdgeIndex> = DenseSlotMap<NI, Node<N, EI>>;

/// A type alias for a [`DenseSlotMap`] that maps a certain key type (the default is [`EdgeIndex`]) to
/// [`Edge`] structures.
///
/// This is intended to be used as an edge map type by the [`Graph`] type.
#[cfg(feature = "slotmap")]
pub type DenseSlotEdgeMap<E, NI = NodeIndex, EI = EdgeIndex> = DenseSlotMap<EI, Edge<E, NI, EI>>;

/// A reexport of [`SecondaryMap`] from the [`slotmap`] crate.
#[cfg(feature = "slotmap")]
pub type SecondarySlotMap<K, V> = SecondaryMap<K, V>;

/// A reexport of [`SparseSecondaryMap`] from the [`slotmap`] crate.
#[cfg(all(feature = "slotmap", feature = "std"))]
pub type SparseSecondarySlotMap<K, V> = SparseSecondaryMap<K, V>;

/// A type alias for a [`SecondaryMap`] with the unit type as the value type.
///
/// This is intended to be used by algorithms on graphs that use [`SlotMap`]s (or equivalents) to
/// back either their node or edge maps.
#[cfg(feature = "slotmap")]
pub type SecondarySlotKeySet<K> = SecondaryMap<K, ()>;

/// A type alias for a [`SparseSecondaryMap`] with the unit type as the value type.
///
/// This is intended to be used by algorithms on graphs that use [`SlotMap`]s (or equivalents) to
/// back either their node or edge maps.
///
/// For differences between a [`SecondaryMap`] and a [`SparseSecondaryMap`] see docs for these
/// types.
#[cfg(all(feature = "slotmap", feature = "std"))]
pub type SparseSecondarySlotKeySet<K> = SparseSecondaryMap<K, ()>;

/// A type alias for a [`BTreeMap`] that maps a certain key type to [`Node`] structures.
///
/// This is intended to be used as a node map type by the [`Graph`] type.
#[cfg(feature = "alloc")]
pub type BTreeNodeMap<N, NI, EI> = BTreeMap<NI, Node<N, EI>>;

/// A type alias for a [`Graph`] that uses a [`BTreeMap`] to store its
/// nodes.
#[cfg(feature = "alloc")]
pub type BTreeNodeGraph<N, E, NI, EI, EM> = Graph<N, E, NI, EI, BTreeNodeMap<N, NI, EI>, EM>;

/// A type alias for a [`FrozenGraph`] that uses a [`BTreeMap`] to
/// store its nodes.
#[cfg(feature = "alloc")]
pub type FrozenBTreeNodeGraph<N, E, NI, EI, EM> =
    FrozenGraph<N, E, NI, EI, BTreeNodeMap<N, NI, EI>, EM>;

/// A type alias for a [`Graph`] that uses a [`BTreeMap`] to store its
/// nodes and a [`SlotMap`] to store its edges.
///
/// The default edge index type is [`EdgeIndex`].
#[cfg(all(feature = "alloc", feature = "slotmap"))]
pub type BTreeSlotMapGraph<N, E, NI, EI = EdgeIndex> =
    BTreeNodeGraph<N, E, NI, EI, SlotEdgeMap<E, NI, EI>>;

/// A type alias for a [`FrozenGraph`] that uses a [`BTreeMap`] to
/// store its nodes and a [`SlotMap`] to store its edges.
///
/// The default edge index type is [`EdgeIndex`].
#[cfg(all(feature = "alloc", feature = "slotmap"))]
pub type FrozenBTreeSlotMapGraph<N, E, NI, EI = EdgeIndex> =
    FrozenBTreeNodeGraph<N, E, NI, EI, SlotEdgeMap<E, NI, EI>>;

/// A type alias for a [`HashMap`] that maps a certain key type to [`Node`] structures.
///
/// This is intended to be used as a node map type by the [`Graph`] type.
///
/// # Custom hashers
///
/// [`HashNodeMap`] supports passing a custom hasher type to [`HashMap`] via its last generic
/// parameter `S`, the default is [`RandomState`] which is also the default type used by the Rust
/// standard library.
#[cfg(feature = "std")]
pub type HashNodeMap<N, NI, EI, S = RandomState> = HashMap<NI, Node<N, EI>, S>;

/// A type alias for a [`Graph`] that uses a [`HashMap`] to store its
/// nodes.
///
/// # Custom hashers
///
/// [`HashNodeGraph`] supports passing a custom hasher type to [`HashMap`] via its last generic
/// parameter `S`, the default is [`RandomState`] which is also the default type used by the Rust
/// standard library.
#[cfg(feature = "std")]
pub type HashNodeGraph<N, E, NI, EI, EM, S = RandomState> =
    Graph<N, E, NI, EI, HashNodeMap<N, NI, EI, S>, EM>;

#[cfg(feature = "std")]
/// A type alias for a [`FrozenGraph`] that uses a [`HashMap`] to store
/// its nodes.
///
/// # Custom hashers
///
/// [`FrozenHashNodeGraph`] supports passing a custom hasher type to [`HashMap`] via its last
/// generic parameter `S`, the default is [`RandomState`] which is also the default type used by the
/// Rust standard library.
pub type FrozenHashNodeGraph<N, E, NI, EI, EM, S = RandomState> =
    FrozenGraph<N, E, NI, EI, HashNodeMap<N, NI, EI, S>, EM>;

/// A type alias for a [`Graph`] that uses a [`HashMap`] to store its
/// nodes and a [`SlotMap`] to store its edges.
///
/// The default edge index type is [`EdgeIndex`].
///
/// # Custom hashers
///
/// [`HashSlotMapGraph`] supports passing a custom hasher type to [`HashMap`] via its last generic
/// parameter `S`, the default is [`RandomState`] which is also the default type used by the Rust
/// standard library.
#[cfg(all(feature = "std", feature = "slotmap"))]
pub type HashSlotMapGraph<N, E, NI, EI = EdgeIndex, S = RandomState> =
    HashNodeGraph<N, E, NI, EI, SlotEdgeMap<E, NI, EI>, S>;

/// A type alias for a [`FrozenGraph`] that uses a [`HashMap`] to store
/// its nodes and a [`SlotMap`] to store its edges.
///
/// The default edge index type is [`EdgeIndex`].
///
/// # Custom hashers
///
/// [`FrozenHashSlotMapGraph`] supports passing a custom hasher type to [`HashMap`] via its last
/// generic parameter `S`, the default is [`RandomState`] which is also the default type used by the
/// Rust standard library.
#[cfg(all(feature = "std", feature = "slotmap"))]
pub type FrozenHashSlotMapGraph<N, E, NI, EI = EdgeIndex, S = RandomState> =
    FrozenHashNodeGraph<N, E, NI, EI, SlotEdgeMap<E, NI, EI>, S>;

/// A type alias for a [`Graph`] that uses [`SlotMap`]s to store nodes and edges.
///
/// The default node and edge index types are [`NodeIndex`] and [`EdgeIndex`].
#[cfg(feature = "slotmap")]
pub type SlotMapGraph<N, E, NI = NodeIndex, EI = EdgeIndex> =
    Graph<N, E, NI, EI, SlotNodeMap<N, NI, EI>, SlotEdgeMap<E, NI, EI>>;

/// A type alias for a [`FrozenGraph`] that uses [`SlotMap`]s to store nodes and edges.
///
/// The default node and edge index types are [`NodeIndex`] and [`EdgeIndex`].
#[cfg(feature = "slotmap")]
pub type FrozenSlotMapGraph<N, E, NI = NodeIndex, EI = EdgeIndex> =
    FrozenGraph<N, E, NI, EI, SlotNodeMap<N, NI, EI>, SlotEdgeMap<E, NI, EI>>;
