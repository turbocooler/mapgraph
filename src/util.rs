#[cfg(feature = "rayon")]
use crate::graph::{Edge, Node};
#[cfg(feature = "alloc")]
use core::iter::Map as IterMap;
#[cfg(feature = "rayon")]
use rayon::iter::Map as ParMap;

#[cfg(feature = "alloc")]
pub(crate) type KeyDerefFn<'a, K, V> = fn((&'a K, &'a V)) -> (K, &'a V);
#[cfg(feature = "alloc")]
pub(crate) type KeyDerefMutFn<'a, K, V> = fn((&'a K, &'a mut V)) -> (K, &'a mut V);
#[cfg(feature = "alloc")]
pub(crate) type KeyMap<'a, I, K, V> = IterMap<I, KeyDerefFn<'a, K, V>>;
#[cfg(feature = "alloc")]
pub(crate) type KeyMapMut<'a, I, K, V> = IterMap<I, KeyDerefMutFn<'a, K, V>>;

#[cfg(feature = "alloc")]
pub(crate) fn map_deref_key<'a, K: Copy, R: 'a>(tuple: (&'a K, R)) -> (K, R) {
    let (&index, value) = tuple;
    (index, value)
}

#[cfg(feature = "rayon")]
type NodeMapFn<NI, EI, N> = for<'a> fn((NI, &'a Node<N, EI>)) -> (NI, &'a N);
#[cfg(feature = "rayon")]
type NodeMapMutFn<NI, EI, N> = for<'a> fn((NI, &'a mut Node<N, EI>)) -> (NI, &'a mut N);
#[cfg(feature = "rayon")]
pub(crate) type EdgeMapFn<NI, EI, E> = for<'a> fn((EI, &'a Edge<E, NI, EI>)) -> (EI, &'a E);
#[cfg(feature = "rayon")]
pub(crate) type EdgeMapMutFn<NI, EI, E> =
    for<'a> fn((EI, &'a mut Edge<E, NI, EI>)) -> (EI, &'a mut E);

#[cfg(feature = "rayon")]
pub(crate) type ParNodeWeightIter<I, NI, EI, N> = ParMap<I, NodeMapFn<NI, EI, N>>;
#[cfg(feature = "rayon")]
pub(crate) type ParNodeWeightIterMut<I, NI, EI, N> = ParMap<I, NodeMapMutFn<NI, EI, N>>;
#[cfg(feature = "rayon")]
pub(crate) type ParEdgeWeightIter<I, NI, EI, E> = ParMap<I, EdgeMapFn<NI, EI, E>>;
#[cfg(feature = "rayon")]
pub(crate) type ParEdgeWeightIterMut<I, NI, EI, E> = ParMap<I, EdgeMapMutFn<NI, EI, E>>;
