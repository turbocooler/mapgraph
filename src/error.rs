//! Contains all error types provided by the crate.

use core::fmt::{self, Debug};

/// An error returned when either the source node index
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum InvalidEdgeError {
    /// A source index for an edge is invalid.
    InvalidSourceIndex,
    /// A destination index for an edge is invalid.
    InvalidDestIndex,
}

impl fmt::Display for InvalidEdgeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            InvalidEdgeError::InvalidSourceIndex => "invalid edge source index",
            InvalidEdgeError::InvalidDestIndex => "invalid edge destination index",
        })
    }
}

#[cfg(feature = "std")]
impl std::error::Error for InvalidEdgeError {}

/// An error returned when trying to add an edge to a [`Graph`](super::Graph).
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub enum AddEdgeError {
    /// Either source or destination index of an added edge were invalid.
    InvalidEdge(InvalidEdgeError),
    /// An edge between the specified nodes exists.
    EdgeExists,
}

impl fmt::Display for AddEdgeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            AddEdgeError::InvalidEdge(e) => fmt::Display::fmt(e, f),
            AddEdgeError::EdgeExists => f.write_str("edge exists"),
        }
    }
}

impl From<InvalidEdgeError> for AddEdgeError {
    fn from(value: InvalidEdgeError) -> Self {
        AddEdgeError::InvalidEdge(value)
    }
}

#[cfg(feature = "std")]
impl std::error::Error for AddEdgeError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            AddEdgeError::InvalidEdge(e) => Some(e),
            AddEdgeError::EdgeExists => None,
        }
    }
}

/// An error returned when a node is added to a graph that uses a map with externally provided keys
/// as a node map and another node exists at the specified index.
#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
pub struct NodeExistsError(pub(crate) ());

impl fmt::Display for NodeExistsError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("node exists")
    }
}

#[cfg(feature = "std")]
impl std::error::Error for NodeExistsError {}

/// An error returned when a node removed from a [`Graph`](crate::Graph) can't be removed.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct RemoveNodeError;

impl fmt::Display for RemoveNodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("node has connected edges and can't be removed")
    }
}

#[cfg(feature = "std")]
impl std::error::Error for RemoveNodeError {}
